﻿using System;
using TestApp.Strings;

class Program
{
    static void Main(string[] args)
    {
        PrintCommands();
        /*Console.WriteLine("Type a string:");
        var str = Console.ReadLine();

        int maxConsecutive = StringHelper.CountUnequal(str);
        Console.WriteLine(maxConsecutive);*/

    }

    static void PrintCommands()
    {
        string opt, text;
        int res = 0;

        Console.WriteLine("Program started..");

        Console.WriteLine("Type any text");
        text = Console.ReadLine();

        Console.WriteLine("Choose one of following options:");
        Console.WriteLine("[1] - Count unequal consecutive characters.");
        Console.WriteLine("[2] - Count maximum of identical Latin aplhabet");
        Console.WriteLine("[3] - Count maximum of identical digits");
        Console.WriteLine("[q] - Quit");

        do
        {
            opt = Console.ReadLine();

            switch (opt)
            {
                case "1":
                    {
                        try
                        {
                            res = StringHelper.CountUnequal(text);
                            Console.WriteLine($"Number of unequal consecutive characters: {res}");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                break;
                case "2":
                    {
                        try
                        {
                            res = StringHelper.CountMaximumRepeatedLetters(text);
                            Console.WriteLine($"Number of identical Latin letters: {res}");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                    break;
                case "3":
                    {
                        try
                        {
                            res = StringHelper.CountMaximumRepeatedDigits(text);
                            Console.WriteLine($"Number of digits: {res}");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                    break;
            }

        }
        while (opt != "q");

    }
}
